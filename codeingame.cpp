#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;
/**
 * Shoot enemies before they collect all the incriminating data!
 * The closer you are to an enemy, the more damage you do but don't get too close or you'll get killed.
 **/
 
int main()
{
    // game loop
    while (1) {
        
        int x;
        int y;
        cin >> x >> y; cin.ignore();
        
        int dataCount;
        cin >> dataCount; cin.ignore();
        
        int data[dataCount][3];
        for (int i = 0; i < dataCount; i++) {
            cin >> data[i][0] >> data[i][1] >> data[i][2]; cin.ignore();
        }
        
        int enemyCount;
        cin >> enemyCount; cin.ignore();
        
        int enemies[enemyCount][7];
        // ID, X, Y, LIFE, DISTANCE, CLOSEST DATA POINT, DISTANCE TO CLOSEST DATAPOINT
        
        int priorityEnemy = -1;
        int closestEnemy = -1;
        int secondClosestEnemy = -1;
        
        bool move = false;
        bool movingToData = false;
        
        int wolffX;
        int wolffY;
        
        int closestEnemyData[4]; //ENEMYID, DATAID, DISTANCE, DISTANCETOWOLFF
        closestEnemyData[0] = -1;
        
        int secondClosestEnemyData[4]; 
        secondClosestEnemyData[0] = -1;
        
        for (int i = 0; i < enemyCount; i++) {
            cin >> enemies[i][0] >> enemies[i][1] >> enemies[i][2] >> enemies[i][3]; cin.ignore();
            int distance = sqrt(pow((enemies[i][1] - x), 2) + pow((enemies[i][2] - y), 2)); //distance from wolff to enemy
            enemies[i][4] = distance;
                        
            if (closestEnemy == -1 || enemies[closestEnemy][4] > distance) {
                closestEnemy = i;
            }
            
            if ((closestEnemy != -1 && secondClosestEnemy == -1) || (distance < enemies[secondClosestEnemy][4] && distance > enemies[closestEnemy][4] && enemies[i][3] < 3000)) {
                secondClosestEnemy = i;
            }
            
            if (enemies[i][3] - (125000/(pow(enemies[i][4], 1.2))) <= 0) {
                priorityEnemy = i;   
            }
            
            for (int j = 0; j < dataCount; j++) {
                int distanceToData = sqrt(pow((enemies[i][1] - data[j][1]), 2) + pow((enemies[i][2] - data[j][2]), 2));
                if (distanceToData < closestEnemyData[2] || closestEnemyData[0] == -1) {
                    if (closestEnemyData[0] != -1) {
                        secondClosestEnemyData[0] = closestEnemyData[0]; 
                        secondClosestEnemyData[1] = closestEnemyData[1];
                        secondClosestEnemyData[2] = closestEnemyData[2];
                        secondClosestEnemyData[3] = closestEnemyData[3];
                    }
                    closestEnemyData[0] = i;
                    closestEnemyData[1] = j;
                    closestEnemyData[2] = distanceToData;
                    closestEnemyData[3] = sqrt(pow((data[j][1] - x), 2) + pow((data[j][2] - y), 2));
                }
            }
            
            /*
            cerr << "enemyId = " << enemies[i][0] << endl;
            cerr << "enemyX = " << enemies[i][1] << endl;
            cerr << "enemyY = " << enemies[i][2] << endl;
            cerr << "enemyLife = " << enemies[i][3] << endl;
            cerr << "distance =" << enemies[i][4] << endl;  
            */
        }
        
        if (enemies[closestEnemy][4] > 4501) {
            if ((closestEnemyData[2] + 1500) < secondClosestEnemyData[2] && closestEnemyData[3] > 4001) {
                movingToData = true;
            } else if (closestEnemyData[3] < 4501) {
                closestEnemy = closestEnemyData[0];
            }
        }
        /*    
        cerr << " closestEnemyData = " << closestEnemyData[2] << endl;
        cerr << " secondClosestEnemyData = " << secondClosestEnemyData[2] << endl;
        cerr << " x = " << x << endl;
        cerr << " y = " << y << endl;
        cerr << " closest enemy = " << closestEnemy << endl;
        */
            
        int damageDealt = 125000 / (pow(enemies[closestEnemy][4], 1.2));  
        if (enemies[closestEnemy][4] < 2501) { // if enemy is too close
            move = true;
            wolffX = x - (enemies[closestEnemy][1] - x);
            wolffY = y - (enemies[closestEnemy][2] - y);
            int xDiff =  sqrt(pow((enemies[closestEnemy][1] - x), 2));
            int yDiff =  sqrt(pow((enemies[closestEnemy][2] - y), 2));
            
            if (secondClosestEnemy != -1) {
                cerr << " moving away from 2 enemies" << endl;
                wolffX = x - (((enemies[closestEnemy][1] + enemies[secondClosestEnemy][1]) / 2) - x);
                wolffY = y - (((enemies[closestEnemy][2] + enemies[secondClosestEnemy][2]) / 2) - y);
            }
            
            //cerr << closestEnemy << " " << secondClosestEnemy << endl;
            //cerr << "closeste = " << enemies[closestEnemy][1] << " " << enemies[closestEnemy][2] << endl;
            //cerr << "secclose = " << enemies[secondClosestEnemy][1] << " " << enemies[secondClosestEnemy][2] << endl;
            //cerr << "wolff = " << wolffX << " " << wolffY << endl;
            
            // if cornered
            if (x < 2000) {
                
                cerr << "got cornered on left";
                
                if ( y < 2000) {
                    
                    cerr << "top, moving out" << endl;
                    
                    if (xDiff < yDiff) {
                        wolffX = x + 2000;
                    } else {
                        wolffY = y + 2000;
                    }
                    
                } else if (y > 7000) {
                    
                     cerr << "bottom, moving out" << endl;
                        
                    if (xDiff < yDiff) {
                        wolffX = x + 2000;
                    } else {
                        wolffY = y - 2000;
                    }
                }
            } else if (x > 14000) {
                
                cerr << "got cornered on left";
                
                if ( y < 2000) {
                    
                    cerr << "top, moving out" << endl;
                    
                    if (xDiff < yDiff) {
                        wolffX = x - 2000;
                    } else {
                        wolffY = y + 2000;
                    }
                } else if (y > 7000) {
                    
                     cerr << "bottom, moving out" << endl;
                        
                    if (xDiff < yDiff) {
                        wolffX = x - 2000;
                    } else {
                        wolffY = y - 2000;
                    }
                }
            }
        } else if ((damageDealt < (enemies[closestEnemy][3] / 2.5) && priorityEnemy == -1 && enemies[closestEnemy][4] > 4501) || (movingToData && priorityEnemy == -1)) {
            move = true;
            
            cerr << "moving closer ";
            
            int moveToX;
            int moveToY;
            
            if (movingToData) {
                cerr << " to data point" << endl;
                moveToX = data[closestEnemyData[1]][1];
                moveToY = data[closestEnemyData[1]][2];
                
                cerr << moveToX << " " << moveToY << endl;
                cerr << enemies[closestEnemyData[0]][1] << " " << enemies[closestEnemyData[0]][2]  << endl;
                
            } else {
                cerr << " to enemy" << endl;
                moveToX = enemies[closestEnemy][1];
                moveToY = enemies[closestEnemy][2];
            }
            
            if (enemies[closestEnemy][1] < x) {
                wolffX = moveToX - 1000;
            } else {
                wolffX = moveToX + 1000;
            }
            if (enemies[closestEnemy][2] < y) {
                wolffY = moveToY - 1000;
            } else {
                wolffY = moveToY + 1000;
            }
        } else if (priorityEnemy >= 0) {
            closestEnemy = priorityEnemy;
        } 
   
/*
            shootEnemy = priorityEnemy;
            if ((distance < 4001 || enemies[shootEnemy][3] > 10)) {
                shootEnemy = -1;
            }
*/
        
        if (move) {
            cout << "MOVE " << wolffX << " " << wolffY << endl;
        } else {
            cout << "SHOOT " << enemies[closestEnemy][0] << endl;
        }
        
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;
        //cout << "MOVE 8000 4500" << endl; // MOVE x y or SHOOT id
    }
}