# How it works
The Accountant Hackathon is a programming competition for which you will have 15 days to imagine the best solution to an action game. Challenge your friends and see how you stack up against worldwide coders!
# Choose your weapon
There are 25+ programming languages to choose from to participate, so go for your favorite or try a more exotic one!
Easy to enter, harder to master
It doesn�t take more than a few minutes to come up with a basic solution, but to get the highest possible score, you will have to optimize your code the smart way.
# Fine tune your strategy
Within the 15 days of the competition, you will be able to fine tune your program whenever you like, as much as you like, to climb in the rankings.
# Rookie? Train first!
New on CodinGame? Try some of our other Optimization Games to practice before the contest.

https://www.codingame.com/hackathon/the-accountant